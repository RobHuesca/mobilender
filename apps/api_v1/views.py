from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.api_v1.models import Pedido
from apps.api_v1.serializers import PedidoSerializer


# Create your views here.


class PedidosViewset(viewsets.ModelViewSet):
    """
        Api administrador de pedidos
        ---
        Parametros GET :
        - nombre: tipo
          descripcion: Tipo de pedido - (1, Centro de distribución) (2, Sucursal) (3, Empresa asociada)
          tipo: int
          requerido: False
        - nombre: cliente
          descripcion: Tipo de cliente - (1, Normal) (2, Plata) (3, Oro) (4, Platino)
          tipo: int
          requerido: False
        - nombre: surtido
          descripcion: Filtrar por si los pedidos han sido surtido - (1, Surtido) (2, No surtido)
          tipo: int
          requerido: False

        ---
        Parametros POST:
        - nombre: cliente
          descripcion: ID del cliente
          tipo: int
          requerido: True
        - nombre: tipo
          descripcion: Tipo de pedido que se está realizando
          tipo: int
          requerido: True
        - nombre: fecha_solicitado
          descripcion: Fecha en que se realizará el pedido
          tipo: date (YYYY-MM-DD)
          requerido: True
        - nombre: articulos
          descripcion: Detallado de los articulos que se están solicitando en el pedido
          tipo: Json ([{"articulo": "18", "cantidad": "3"},{"articulo": "12", "cantidad": "6"},
          { "articulo": "11", "cantidad": "7"}])
          requerido: True
        - nombre: almacen
          descripcion: Nombre del almacen que surte el pedido cuando el tipo de
          pedido es 1 (Centro de distribucion)
          tipo: String
          requerido: False
        - nombre: referencia
          descripcion: Referencia de la sucursal o empresa cuando el tipo de pedido
          es 2 o 3 (Sucursal, Empresa asociada)
          tipo: String
          requerido: False
        - nombre: codigo (socio, sucursal)
          descripcion: Codigo de socio/sucursal cuando el tipo de pedido es 2 o 3 (Sucursal, Empresa asociada)
          tipo: int
          requerido: False
    """
    queryset = Pedido.objects.all()
    serializer_class = PedidoSerializer
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'post']

    def get_queryset(self):
        tipo = self.request.query_params.get('tipo')
        cliente = self.request.query_params.get('cliente')
        surtido = self.request.query_params.get('surtido')
        queryset = self.queryset

        if tipo:
            queryset = queryset.filter(tipo=tipo)
        if cliente:
            queryset = queryset.filter(cliente__tipo_cliente=cliente)
        if surtido == '1':
            queryset = queryset.filter(fecha_surtido__isnull=False)
        elif surtido == '2':
            queryset = queryset.filter(fecha_surtido__isnull=True)
        return queryset


class DashboardAPI(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        data_pedidos = []
        data_cliente = []
        pedidos = Pedido.objects.all()

        """
         Total de pedidos filtrado por tipos de pedidos
        """
        t_distribucion = pedidos.filter(tipo=1).count()
        t_sucursal = pedidos.filter(tipo=2).count()
        t_empresa = pedidos.filter(tipo=3).count()
        data_pedidos.append({"distribucion": t_distribucion, "sucursal": t_sucursal, "empresa": t_empresa})

        """
         Total de pedidos filtrado por tipo de cliente
        """
        t_normal = pedidos.filter(cliente__tipo_cliente=1).count()
        t_plata = pedidos.filter(cliente__tipo_cliente=2).count()
        t_oro = pedidos.filter(cliente__tipo_cliente=3).count()
        t_platino = pedidos.filter(cliente__tipo_cliente=4).count()
        data_cliente.append({"normal": t_normal, "plata": t_plata, "oro": t_oro, "platino": t_platino})

        data = {"total_pedidos": data_pedidos, "total_clientes": data_cliente}
        return Response(data)
