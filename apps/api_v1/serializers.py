from rest_framework import serializers
from apps.api_v1.models import Pedido, PedidoArticulo, Articulo, Cliente
from django.db import transaction
import json


class ClienteSerializer(serializers.ModelSerializer):
    tipo = serializers.CharField(source='get_tipo_cliente_display')

    class Meta:
        model = Cliente
        fields = ['nombre', 'codigo', 'direccion', 'tipo']


class ArticuloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articulo
        fields = '__all__'


class PedidoArticuloSerializer(serializers.ModelSerializer):
    articulo = ArticuloSerializer()

    class Meta:
        model = PedidoArticulo
        fields = '__all__'


class PedidoSerializer(serializers.ModelSerializer):
    articulos = serializers.SerializerMethodField()
    cliente_ob = serializers.SerializerMethodField()
    tipo_text = serializers.CharField(source='get_tipo_display', required=False)

    class Meta:
        model = Pedido
        fields = ['pk', 'cliente', 'cliente_ob', 'tipo', 'tipo_text', 'fecha_solicitado',
                  'fecha_surtido', 'urgente', 'almacen', 'referencia', 'codigo', 'articulos']
        read_only_fields = ['pk']

    def get_articulos(self, obj):
        arts = PedidoArticulo.objects.filter(pedido_id=obj.pk)
        datos = PedidoArticuloSerializer(arts, many=True)
        return datos.data

    def get_cliente_ob(self, obj):
        object = Cliente.objects.get(id=obj.cliente_id)
        cliente = ClienteSerializer(object, many=False)
        return cliente.data

    @transaction.atomic()
    def create(self, validated_data):
        request_data = self.context['request'].data
        instance = Pedido(**validated_data)
        instance.save()
        if request_data['articulos']:
            for art in json.loads(request_data['articulos']):
                PedidoArticulo.objects.create(pedido_id=instance.pk,articulo_id=art["articulo"],
                                              cantidad=art["cantidad"])
        return instance
