from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.api_v1.views import PedidosViewset, DashboardAPI

app_name = 'api_v1'

router = DefaultRouter()
router.register(r'pedidos', PedidosViewset, basename='pedidos')

urlpatterns = [
    path('', include(router.urls)),
    path('dashboard/', DashboardAPI.as_view(), name='dashboard')
]
