from django.db import models

# Create your models here.


class Cliente(models.Model):
    TIPO_USUARIO = (
        (1, "NORMAL"),
        (2, "PLATA"),
        (3, "ORO"),
        (4, "PLATINO"),
    )
    nombre = models.CharField(max_length=255)
    codigo = models.CharField(max_length=15)
    fotografia = models.FileField()
    direccion = models.CharField(max_length=255)
    tipo_cliente = models.PositiveSmallIntegerField(choices=TIPO_USUARIO)

    def __str__(self):
        return '{} - {} - {}'.format(self.codigo, self.nombre, self.tipo_cliente)

    class Meta:
        default_permissions = ()


class Proveedor(models.Model):
    nombre = models.CharField(max_length=100)
    direccion = models.CharField(max_length=150)

    def __str__(self):
        return '{}'.format(self.nombre)

    class Meta:
        default_permissions = ()


class Articulo(models.Model):
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE)
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=150)
    precio = models.DecimalField(max_digits=7, decimal_places=2)

    def __str__(self):
        return '{} - {} - {}'.format(self.codigo, self.descripcion, self.proveedor)

    class Meta:
        default_permissions = ()


class Pedido(models.Model):
    TIPO_PEDIDO = (
        (1, "CENTRO DE DISTRIBUCION"),
        (2, "SUCURSAL"),
        (3, "EMPRESA ASOCIADA"),
    )
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    fecha_solicitado = models.DateField()
    fecha_surtido = models.DateField(null=True, blank=True)
    urgente = models.BooleanField(default=False)
    tipo = models.PositiveSmallIntegerField(choices=TIPO_PEDIDO)
    almacen = models.CharField(max_length=100, null=True, blank=True)
    referencia = models.CharField(max_length=100, null=True, blank=True)
    codigo = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.cliente.nombre, self.tipo)

    class Meta:
        default_permissions = ()


class PedidoArticulo(models.Model):
    pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE)
    articulo = models.ForeignKey(Articulo, on_delete=models.CASCADE)
    cantidad = models.IntegerField()

    def __str__(self):
        return '{} {}'.format(self.articulo.descripcion, self.pedido.cliente.nombre)

    class Meta:
        default_permissions = ()
