from django.contrib import admin
from .models import Proveedor, Cliente, Articulo, Pedido, PedidoArticulo
# Register your models here.
admin.site.register(Proveedor)
admin.site.register(Cliente)
admin.site.register(Articulo)
admin.site.register(Pedido)
admin.site.register(PedidoArticulo)
