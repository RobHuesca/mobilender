""" Pedidos Tests"""

from django.test import TestCase

from apps.api_v1.models import Pedido, Cliente, Proveedor, Articulo, PedidoArticulo


class PedidoTestCase(TestCase):
    def setUp(self):
        self.cliente = Cliente.objects.create(
            nombre='Roberto',
            codigo='009OIIED0',
            fotografia='image.png',
            direccion='conocida',
            tipo_cliente=1
        )
        self.proveedor = Proveedor.objects.create(
            nombre='Tecnologias Diaz',
            direccion='Travessera Aitor, 08, 6º B'
        )
        self.articulo = Articulo.objects.create(
            proveedor=self.proveedor,
            codigo='300760',
            descripcion='Teclado inalámbrico',
            precio=209.89
        )

    def test_articulo(self):
        articulo = Articulo.objects.create(
            proveedor=self.proveedor,
            codigo='300760',
            descripcion='Teclado inalámbrico',
            precio=209.89
        )
        self.assertIsNotNone(articulo)

    def test_proveedor(self):
        proveedor = Proveedor.objects.create(
            nombre='Tecnologias Diaz',
            direccion='Travessera Aitor, 08, 6º B'
        )

    def test_pedido(self):
        pedido = Pedido.objects.create(
            cliente=self.cliente,
            fecha_solicitado='2021-05-12',
            urgente=False,
            tipo=1,
            almacen='QW344D'
        )
        detalle = PedidoArticulo.objects.create(
            pedido=pedido,
            articulo=self.articulo,
            cantidad=5
        )
        self.assertIsNotNone(detalle)





