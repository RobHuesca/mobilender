# API Mobilender

Prueba para vacante desarrollador fullstack realizada por Roberto Huesca

## Instalación BackEnd

El proyecto está en un contenedor [docker](https://www.docker.com) por lo tanto para ejecutarlo se necesita primero correr el siguiente comando para crear la imagen

```bash
docker compose -f local.yml build
```
Una vez creada la imagen se debe ejecutar el contenedor con el siguiente comando
```bash
docker compose -f local.yml up
```
## Uso
Una vez ejecutado el contenedor podemos acceder a la documentacion de la API a través de la direccion:
```bash
localhost:8000
``` 
Para poder obtener datos de la api necesitamos cargar el fixture que se encuentra dentro de la carpeta db ejecutando el comando:
```bash
docker compose -f local.yml run --rm django python manage.py loaddata db/db.json
``` 
## Tests
Para poder ejecutar el test de creacion de pedidos ejecutar el siguiente comando:
```bash
docker compose -f local.yml run --rm django pytest
``` 

## Instalacion FrontEnd
El proyecto de frontend está desarrollado con [Vuejs](https://vuejs.org) por lo tanto para ejecutarlo se debe tener previamente instalado NodeJS y NPM. Una vez teniendo lo necesario ejecutar el comando:
```bash
npm install 
``` 
Eso instalará las dependencias para el proyecto, una vez finalizado ejecutar:
```bash
npm run serve 
``` 
## Uso

Para acceder al sitio deberá ingresar a la siguiente direccion:
```bash
http://localhost:8080
``` 
