"""Main URLs module."""

from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from django.contrib import admin

from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Mobilender API')

urlpatterns = [
    # Django REST Swagger
    path('', schema_view, name='api-doc'),

    # Django Admin
    path(settings.ADMIN_URL, admin.site.urls),

    # Pedidos Url's
    path('v1/', include('apps.api_v1.urls', namespace='api_v1')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
