# Base
pytz==2018.7
python-slugify==1.2.6
Pillow==5.3.0
psycopg2==2.7.4 --no-binary psycopg2

# Django
django==3.2

# Environment
django-environ==0.4.5

# Passwords security
argon2-cffi==18.3.0

# Static files
whitenoise==4.1.2

# Swagger
django-rest-swagger==2.2.0

# Rest-Framework
djangorestframework==3.12.4

# Django Cors Headers
django-cors-headers==3.7.0
